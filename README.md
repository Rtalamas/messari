# Messari API
Messari provides a free API for crypto prices, market data metrics, on-chain metrics, and qualitative information (asset profiles).

This documentation will provide the basic steps to start using messari’s python library.

## Installing messari
The first step is installing messari. messari is a python project, so it can be installed like any other python library. Every Operating System should have Python pre-installed, so you should just have to run:

```
pip install messari
```

## Docs

To open the offical docs go [here](docs/build/html/index.html)
