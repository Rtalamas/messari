from string import Template
from typing import List

import pandas as pd

from messari.utils import generate_urls
from messari.utils import token_terminal_request
from messari.utils import response_to_df

"""
The revenue module is powered by the Token Terminal API.

Need to add tests
"""

# Messari's API key
API_KEY = '610b30be-e8b5-4b64-a27d-d59c94132ee4'
BASE_URL = 'https://api.tokenterminal.com/v1/projects'


def get_project_ids():
    """
    Returns all the project ids available in Token Terminal

    Returns
    -------
        List
            List of token ids.
    """
    url = BASE_URL
    data = token_terminal_request(url, API_KEY)
    return [x['project_id'] for x in data]


def get_all_protocol_data(to_dataframe=True):
    """
    Returns an overview of latest data for all projects, ranging from metadata
    such as launch dates, logos brand colors and Twitter followers to more
    fundamental metrics such as Revenue, GMV, TVL and P/S ratios.

    The data is updated every 10 minutes.

    Parameters
    ----------
        to_dataframe: bool
            Return data as pandas DataFrame or JSON. Default is set to JSON.
    Returns
    -------
        dict, DataFrame
            Dictionary or pandas DataFrame of asset data.
    """
    url = BASE_URL
    data = token_terminal_request(url, API_KEY)
    if to_dataframe:
        return pd.DataFrame(data)
    return data


def get_protocol_data(protocol_id: str, to_dataframe: bool = True):
    """
    Returns a time series of the latest data for a given project, ranging from metadata
    such as Twitter followers to more fundamental metrics such as Revenue, GMV, TVL and P/S ratios.

    Parameters
    ----------
        protocol_id: str
            String of protocol ID
        to_dataframe: bool
            Return data as pandas DataFrame or JSON. Default is set to JSON.
    Returns
    -------
        dict, DataFrame
            Dictionary or pandas DataFrame of asset data.
    """
    url = f'{BASE_URL}/{protocol_id}/metrics'
    data = token_terminal_request(url, API_KEY)
    if to_dataframe:
        return response_to_df(data)
    return data


def get_historical_metric_data(protocol_ids: List, metric: str):
    """
    Returns the time series of a specified metric for a given list of project.

    Parameters
    ----------
        protocol_ids: list
            List of project IDs
        metric: str
            Single metric string to filter data.
            Available metrics include:
                - price,
                - market_cap
                - market_cap_circulating
                - market_cap_fully_diluted
                - volume
                - vol_mc
                - pe
                - ps
                - tvl
                - gmv
                - revenue
                - revenue_supply_side
                - revenue_protocol
                - token_incentives

    Returns
    -------
        DataFrame
            pandas DataFrame with asset metric data.
    """
    url = Template(f'{BASE_URL}/$asset_key/metrics')
    urls = generate_urls(url, protocol_ids)
    metric_df = pd.DataFrame()
    for url, protocol_id in zip(urls, protocol_ids):
        data = token_terminal_request(url, API_KEY)
        data_df = response_to_df(data)
        single_metric_df = data_df[metric].to_frame()
        single_metric_df.columns = [protocol_id]
        metric_df = metric_df.join(single_metric_df, how='outer')
    return metric_df
